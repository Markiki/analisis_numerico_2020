# import math
import numpy as np # formato doble

def doble(x):
    m = 0
    x_ant = np.float64(x)
    x = np.float64(2*x)
    while x_ant < x:
        x_ant = x
        x = np.float64(2 * x)
        m += 1
        print("Iteracion : {}  Penultimo Valor : {}  Ultimo Valor : {}".format(m,x_ant,x))
    return x_ant, x, m

xs = doble(1.0)

print(xs)