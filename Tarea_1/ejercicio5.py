import numpy as np
from scipy.linalg import hilbert

def subMatriz(A, f, c):
    ###
    # Obtiene la submatriz de la Matriz A,
    # quitando la fila f y la columna c
    ###
    Afil = len(A);
    Acol = len(A[0])

    SubMatriz = np.zeros([(Afil - 1) * (Acol - 1)])
    indice = 0

    for i in range(Afil):
        for j in range(Acol):
            if i != f and j != c:
                SubMatriz[indice] = A[i][j]
                indice += 1

    return SubMatriz.reshape([Afil - 1, Acol - 1])


def determinante(A):
    ###
    # Calcula el determinante de la Matriz A
    # de forma Recursiva
    ###
    rows, cols = len(A), len(A[0])
    if rows != cols:
        print('La matriz no es cuadrada')
        return None
    elif rows == 2:
        return (A[0][0] * A[1][1] - A[1][0] * A[0][1])
    elif rows == 1:
        return A[0][0]
    else:
        det = 0
        for i in range(rows):
            signo = (-1) ** (i + 1)
            for j in range(cols):
                signo *= -1
                det += signo * A[i][j] * determinante(subMatriz(A, i, j))
        return det


def inversa(A):
    ###
    # Obtiene la matriz inversa de
    # la matriz A
    ###
    det = determinante(A)
    if det == 0:
        print('La Matriz no tiene Inversa')
        return None

    inversa = np.zeros([len(A), len(A[0])])
    for i in range(len(A)):
        signo = (-1) ** (i + 1)
        for j in range(len(A[0])):
            signo *= -1
            # indices j e i cambiados de orden para trasponer la matriz adjunta
            inversa[j][i] = signo * determinante(subMatriz(A, i, j))

    inversa *= (1 / det)
    return inversa


def norma_inf(A):
    ###
    # Obtiene la norma infinito
    # de la matriz A
    ###
    maximo = 0
    for i in range(len(A)):
        suma = 0

        for j in range(len(A[0])):
            suma += abs(A[i][j])

        maximo = suma if suma > maximo else maximo

    return maximo


def condicion(A):
    ###
    # obtiene el numero de condicion
    # de la matriz A
    ###
    norma_A = norma_inf(A)
    norma_A_trans = norma_inf(inversa(A))

    return norma_A * norma_A_trans

 # Matriz de Hlbert n = 5
H_5 = hilbert(5)

# numero de condicionde de H_5
print("Numero de condicion = ",condicion(H_5))