import math
import numpy as np # float32, precision simple

def stirling(n):
    pi = np.float32(math.pi)
    e = np.float32(math.exp(1))
    x = np.float32(2*pi*n)
    y= np.float32(n/e)
    raiz = np.float32(math.sqrt(x))
    potencia = np.float32(pow(y,n))
    return np.float32(raiz*potencia)

def compare_errors(n):
    values = range(0,n+1)
    factorial = [math.factorial(i) for i in values]
    stirling_aprox = [stirling(i) for i in values]
    errors = [abs(factorial[i]-stirling_aprox[i]) for i in values]
    relative_errors = [(errors[i]/abs(factorial[i])) for i in values]

    print("{:>25s} {:>25s} {:>25s} {:>25s} {:>25s}".format('N','Factorial(N)','Stirling(N)','Error Absoluto','Error Relativo'))
    for i in values:
        print("{:>25} {:>25} {:>25} {:>25} {:>25}".format(i,factorial[i],stirling_aprox[i],errors[i],relative_errors[i]))


compare_errors(20)