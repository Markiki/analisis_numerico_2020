import numpy as np
from scipy.linalg import hilbert, lu_factor, lu_solve
import matplotlib.pyplot as plt

def hilbert_matrix(n):
  print('{:>5s}  {:>20s} {:>30s}'.format("N","Determinante", "Numero de Condicion"))
  for i in range(1,n+1):
    h = hilbert(i)
    print('{:>5d} {:>20f} {:>30f}'.format(i,np.linalg.det(h), np.linalg.cond(h, p = np.inf)))

hilbert_matrix(30)

n = range(1,31)
c = [np.log(np.linalg.cond(hilbert(i), p = np.inf)) for i in n]
plt.plot(n,c)


def solucion_hilbert_LU():
  print("{:>5} {:>25}".format('N', '||x-u||inf'))
  for i in range(5, 31):
    u = np.ones(i)  # vector de unos
    H = hilbert(i)  # Matriz de hilbert
    b = H.dot(u)  # b = Hu

    # Facotizacion LU
    lu, piv = lu_factor(H)

    # Solcucion del sistema Hx = LUx = b
    x = lu_solve((lu, piv), b)

    print("{:>5} {:>25}".format(i, np.linalg.norm(x - u, ord=np.inf)))

solucion_hilbert_LU()