import math
import numpy as np

def e_taylor(x = 1.0, n = 25, paro = 10e-6):
    if x == 0:
        return 1

    e = 0
    signo = x > 0
    x = abs(x)

    for i in range(0,n+1):
        termino = (x**i)/math.factorial(i)
        e += termino
        if termino < paro:
            break

    return (e if signo else 1/e)

def tabla_exp(valores, n = 25, paro = 10e-6):
    if type(valores) in [type(1), type(1.0)]:
        valores = [valores]

    print("{:>5s}  {:>15s}  {:>15s}".format("X", "Mi rutina", "Python"))
    for valor in valores:
        print("{:>5d}  {:>15.7E}  {:>15.7E}".format(valor, e_taylor(valor,n,paro), math.exp(valor)))


# tabla_exp([-25,-10,-5,5,10,12])

def derivada_exp(x = 0, y = 2, n = 20):
    print("{:>5s}  {:>15s}  {:>15s} {:>15s}".format('Valor', 'Cociente 1', 'Cociente 2', 'Diferencia'))
    for i in range(1,n+1):
        h = y**-i
        c1 = (e_taylor(x+h)-e_taylor(x))/h
        c2 = (e_taylor(x+h)-e_taylor(x-h))/(2*h)
        diff = c1-c2
        print("{:>5d}  {:>15f}  {:>15f}  {:>15f}".format(i, c1, c2, diff))

derivada_exp()